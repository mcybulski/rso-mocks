var express = require("express");
var https = require('https');
var fs = require('fs');
var bodyParser = require("body-parser");

var valueJSON = require('./json/value.json');
var creditJSON = require('./json/credit.json');
var confirmJSON = require('./json/confirm.json');
var peselRgx = /\/rso\/obligations\/\d{11}\//;

var privateKey = fs.readFileSync('cert/key.pem');
var certificate = fs.readFileSync('cert/cert.pem');
var credentials = {key: privateKey, cert: certificate};

var app = express();
var httpsServer = https.createServer(credentials, app);

app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get(peselRgx ,function(req,res){
  res.write(JSON.stringify(valueJSON));
  setTimeout(function() {
    res.send();
  }, 2000);
});

app.post('/rso/obligations/', function(req,res){
  res.write(JSON.stringify(creditJSON));
  res.send();
});

app.post(peselRgx ,function(req,res){
  res.write(JSON.stringify(confirmJSON));
  res.send();
});

httpsServer.listen(3000, function() {
  console.log("Started on PORT 3000");
});
