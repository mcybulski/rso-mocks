# README #

### Uruchomienie ###
Do działania mocków potrzebne jest środowisko node.js wraz z npm.
Przed uruchomieniem konieczne jest również pobranie wymaganych zależności:

```
npm install
```

w folderze /mock

Mock (plik /mock/mock.js) uruchamiamy przy pomocy node.js
```
node mock.js 
```
Folder /mock/json wraz z plikami musi znajdować się w tej samej lokacji, co plik mock.js.

### Użytkowanie ###
Mock jest jeden, bo przy aktalnym API (identycznym dla obu warstw) może udawać obie warstwy. Mock działa na adresie 127.0.0.1, port 3000.